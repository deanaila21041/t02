public class Main {
    public static void main(String[] args){
        LCD lcd1 = new LCD();
        lcd1.setStatus("Freeze");
        lcd1.setVolume(30);
        lcd1.setBrightness(15);
        lcd1.setCable("DVI");

        lcd1.displayMessage();
    }
    
}
